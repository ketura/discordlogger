﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace DiscordLogger
{
	public class LoggerCommands
	{
		public static bool Logging { get; set; }

		private async Task<bool> CheckLogging(CommandContext ctx)
		{
			if (Logging)
			{
				ctx.RespondAsync($"Logging is currently in progress.  Please wait, {ctx.User.Mention}.");
				return true;
			}
			else
			{
				Logging = true;
				var members = await ctx.Guild.GetAllMembersAsync();
				var logger = Logger.GetLogger(ctx);

				foreach (var member in members)
				{
					logger.Members[member.Id] = member;
				}
				return false;
			}
		}


		[Command("log")]
		public async Task Log(CommandContext ctx)
		{
			if (await CheckLogging(ctx))
				return;

			var channels = ctx.Message.MentionedChannels.ToList();
			if (channels.Count() > 0)
			{
				await ctx.RespondAsync($"Logging channels: {string.Join(", ", channels.Select(x => x.Mention).ToArray())}, {ctx.User.Mention}.");
			}
			else
			{
				await ctx.RespondAsync($"Logging {ctx.Channel.Mention}, {ctx.User.Mention}.");
				channels.Add(ctx.Channel);
			}

			Logger.GetLogger().GetMessages(channels);
		}

		[Command("logall")]
		public async Task LogAll(CommandContext ctx)
		{
			if (await CheckLogging(ctx))
				return;

			await ctx.RespondAsync($"Logging all channels, {ctx.User.Mention}.");

			Logger.GetLogger().GetMessages(ctx.Guild.Channels);
		}
	}
}
