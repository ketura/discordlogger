﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using DSharpPlus;
using DSharpPlus.CommandsNext;

namespace DiscordLogger
{
	public class Program
	{

		static DiscordClient Client;
		static CommandsNextModule Commands;

		static void Main(string[] args)
		{
			MainAsync(args).ConfigureAwait(false).GetAwaiter().GetResult();
		}

		static async Task MainAsync(string[] args)
		{
			Client = new DiscordClient(new DiscordConfiguration
			{
				Token = "NDQ3MjM1MjU2OTI0MzczMDAz.DeEv4Q.qLbTKZIxIrrgUpRTyitbH7Uqlkw",
				TokenType = TokenType.Bot,
				UseInternalLogHandler = true,
				LogLevel = LogLevel.Debug

			});

			Logger.GetLogger().Client = Client;

			Commands = Client.UseCommandsNext(new CommandsNextConfiguration
			{
				StringPrefix = "!!"
			});
			

			Commands.RegisterCommands<LoggerCommands>();

			await Client.ConnectAsync();
			await Task.Delay(-1);
		}
	}
}
