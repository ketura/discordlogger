﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using System.Configuration;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;


namespace DiscordLogger
{
	public class Logger
	{
		private static Logger Instance { get; set; }

		public DiscordClient Client { get; set; }

    private List<DiscordChannel> ChannelsToLog { get; set; }
		public Dictionary<ulong, DiscordMember> Members { get; set; }
    private DateTime StartDate { get; set; }
    private DateTime EndDate { get; set; }
		private string LogPath { get; set; }

    private bool Connected { get; set; }

		private string BotToken { get; set; }
		private CommandContext Context { get; set; }

		public static Logger GetLogger(CommandContext ctx=null)
		{
			Instance = Instance ?? new Logger();
			Instance.Context = ctx ?? Instance.Context;
			return Instance;
		}

		public Logger()
		{
			Instance = this;
			ChannelsToLog = new List<DiscordChannel>();
			Members = new Dictionary<ulong, DiscordMember>();
			BotToken = ConfigurationManager.AppSettings["BotToken"];
			StartDate = DateTime.ParseExact(ConfigurationManager.AppSettings["StartDate"], "yyyy-MM-dd", CultureInfo.InvariantCulture);
			EndDate = DateTime.ParseExact(ConfigurationManager.AppSettings["EndDate"], "yyyy-MM-dd", CultureInfo.InvariantCulture);
			LogPath = Path.GetFullPath(ConfigurationManager.AppSettings["LogDirectory"]);
		}

		public async Task GetMessages(IEnumerable<DiscordChannel> channels)
		{
			ChannelsToLog = channels.ToList();
			foreach (var channel in channels.Distinct())
			{
				RetrieveMessagesToDate(channel, StartDate, EndDate);
			}

			Log("\nAll requested channels had logging kicked off.");
		}

		private void Log(string message)
		{
			Client.DebugLogger.LogMessage(LogLevel.Info, "discord_logger", message, DateTime.Now);
		}

		private void LogError(string message)
		{
			Client.DebugLogger.LogMessage(LogLevel.Error, "discord_logger", message, DateTime.Now);
		}

		public async Task RetrieveMessagesToDate(DiscordChannel channel, DateTime startDate, DateTime endDate)
		{
      Log($"Attempting to export all messages from #{channel.Name} sent between {startDate.ToShortDateString()} and {endDate.ToShortDateString()}...");
			ulong currentPlace = channel.LastMessageId;

			List<DiscordMessage> response = new List<DiscordMessage>();
			Dictionary<DateTime, List<DiscordMessage>> Messages = new Dictionary<DateTime, List<DiscordMessage>>();

      //We start from the most recent message and work backwards
			bool foundStart = false;
      long messagesRecieved = 0;

			while (!foundStart)
			{
				response = (await channel.GetMessagesAsync(before: currentPlace)).ToList();
				messagesRecieved += response.Count();

        await Task.Delay(20); //Let's keep ourselves to 5000 messages per second, shall we.
        if(messagesRecieved % 1000 == 1)
        {
					Console.Write($"\rRecieved {messagesRecieved} messages...");
        }

				if (response.Count() == 0)
				{
					Console.Write($"\nReached the end of the message record.");
					break;
				}

				foreach (var m in response)
				{
					if (m.Timestamp.Date < startDate)
					{
						Log($"\nReached a message with timestamp {m.Timestamp}, which is past the provided date.");
						foundStart = true;
						break;
					}

          if (m.Timestamp.Date <= endDate)
          {
						DateTime date = m.Timestamp.Date;
						if (!Messages.ContainsKey(date))
							Messages[date] = new List<DiscordMessage>();

						Messages[date].Add(m);
					}
				}

				currentPlace = response.Last().Id;
			}

			Log($"Found {Messages.Sum(x => x.Value.Count())} messages sent within the provided date range.");
			await WriteMessages(channel.Guild.Name + "_" + channel.Name, Messages);

			ChannelsToLog.Remove(channel);
			await Client.SendMessageAsync(Context.Channel, $"{channel.Mention} logging complete, {Context.User.Mention}.");

			if (ChannelsToLog.Count == 0)
			{
				await Client.SendMessageAsync(Context.Channel, $"All requested logging complete, {Context.User.Mention}.");
				LoggerCommands.Logging = false;
			}
		}

		public async Task WriteMessages(string logPreamble, Dictionary<DateTime, List<DiscordMessage>> messages)
		{
			Log($"Exporting logs to {messages.Keys.Count()} discrete files...");

      string unknownName = "<UNKNOWN USER>";

			foreach (var pair in messages)
			{
				string currentUser = null;
				string currentTime = "";
				string output = "";

				pair.Value.Reverse();
				foreach (var m in pair.Value)
				{
          //Damn usernames not coming through for some reason.
          if (m.Author == null)
          {
            if (currentUser != unknownName || m.Timestamp.LocalDateTime.ToShortTimeString() != currentTime)
            {
              currentUser = unknownName;
              currentTime = m.Timestamp.LocalDateTime.ToShortTimeString();
              output += $"\n[{currentUser}] - {currentTime}\n";
            }
          }
					else if (m.Author.Username != currentUser || m.Timestamp.LocalDateTime.ToShortTimeString() != currentTime)
					{
						if(Members.ContainsKey(m.Author.Id))
						{
							currentUser = Members[m.Author.Id].DisplayName;
						}
						else
						{
							currentUser = m.Author.Username;
						}
						currentTime = m.Timestamp.LocalDateTime.ToShortTimeString();
						output += $"\n[{currentUser}] - {currentTime}\n";
					}

					string text = m.Content;
					if (m.MentionedUsers != null && m.MentionedUsers.Count() > 0)
					{
						foreach (var u in m.MentionedUsers)
						{
							string username = unknownName;
							if (u != null)
							{
								username = $"{u.Username}#{u.Discriminator}";
								string mention = u.Mention;

								if (u.Mention.Contains("!"))
								{
									if (Members.ContainsKey(u.Id))
									{
										username = Members[u.Id].DisplayName;
									}
									else
									{
										mention = mention.Replace("!", "");
									}
								}
								
								//this is a mess, but it's a bit of a pain to figure out the various combinations of whether or not the message mention will have !
								// in conjunction with the user's mention property having !, so we're just nuking all of them with brute force.

								text = text.Replace(mention, $"@{username}");
								mention = mention.Replace("!", "");
								text = text.Replace(mention, $"@{username}");
								mention = mention.Replace("<@", "<@!");
								text = text.Replace(mention, $"@{username}");

							}
						}
					}

					if (m.MentionedChannels != null && m.MentionedChannels.Count() > 0)
					{
						foreach (var c in m.MentionedChannels)
						{
							//this is dumb, but I don't want to have to manually parse the message for channels that /weren't/ deleted.
							if (c == null)
								continue;

							text = text.Replace(c.Mention, $"#{c.Name}");
						}
					}

          if(m.MentionedRoles != null && m.MentionedRoles.Count() > 0)
          {
            foreach(var r in m.MentionedRoles)
            {
							//this is dumb, but I don't want to have to manually parse the message for roles that /weren't/ deleted.
							if (r == null)
								continue;

              text = text.Replace(r.Mention, $"@{r.Name}");
            }
          }

					if(m.Attachments != null && m.Attachments.Count > 0)
					{
						foreach(var attach in m.Attachments)
						{
							if (String.IsNullOrWhiteSpace(text))
							{
								text += attach.Url;
							}
							else
							{
								text += $"\n{attach.Url}";
							}
							
						}
					}

					if (m.IsEdited)
					{
						text += " <edited>";
					}

					output += text + "\n";
				}
				string filename = logPreamble + "_" + pair.Key.ToString("yyyy-MM-dd") + ".txt";
				filename = GetSafeFilename(filename);

        try
        {
					System.IO.Directory.CreateDirectory(LogPath);
					File.WriteAllText(Path.Combine(LogPath, filename), output);
        }
        catch(Exception ex)
        {
          LogError($"Error while attempting to write {filename}:\n\n{ex.ToString()}");
          //add abort/retry/ignore options here?
          return;
        }

        Log($"Wrote to {filename} successfully.");
				
			}

			Log($"{logPreamble} completed logging.");
		}

		public string GetSafeFilename(string filename)
		{
			return string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
		}

		

	}

}
