﻿
How to use this logger for your private server:
===============================================

1. Create a bot application and user.  Follow the instructions here to do so: https://dsharpplus.emzi0767.com/articles/getting_started.html
2. Copy the App Bot User token (you have to click on "click to reveal" to show it) and paste it into the DiscordLogger.exe.config in the "BotToken" entry.
3. Copy the Client ID from the top of your application page and paste it into the following URL: https://discordapp.com/oauth2/authorize/?permissions=68608&scope=bot&client_id=CLIENT_ID_HERE
4. Follow the URL you modified and add the bot to a server you have Manage Server permissions on.
5. Once the bot is added, run DiscordLogger.exe.  
		
		
Your bot is now ready to use.  Note that the bot will only be active so long as you have DiscordLogger.exe running with the applicable BotToken entry made.

You can configure the date range to be logged (put the dates far in the future or the past if you want to always log everything) by changing the StartDate and EndDate entries in the DiscordLogger.exe.config.  

You can also configure the directory that log files are saved to by changing the LogDirectory entry.

To actually invoke logging, use one of the following commands within Discord itself:

* **!!log #channel1 #channel2**.  Every channel that you mention (as in, use the # character and cause it to become a link) will be logged.  Alternatively, if there are no channel mentions the current channel will be logged.
* **!!logall**.  Logs all channels.

Logs are saved to the configured directory, one per date, with timestamps (and dates) linked to the time zone of the computer running DiscordLogger.